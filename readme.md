## Group memebers:
- Patrick Henning

## How to run: 
1. Install Rust: https://www.rust-lang.org/en-US/install.html
2. Install libsodium: Available via most package managers. Otherwise, see: https://download.libsodium.org/doc/installation/
3. cd into 'server' folder and run `cargo run`
4. cd into client folder and run `cargo run <port number> <file names>`