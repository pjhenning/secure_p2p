#[macro_use]
extern crate serde_derive;
extern crate sodiumoxide;

use sodiumoxide::crypto::{box_, secretbox};
use std::io::prelude::*;

use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

#[derive(Serialize, Deserialize)]
pub struct SessionRequestData {
    pub username: String,
    pub password: String,
}

pub type SessionID = [u8; 4];

pub type SessionMap = std::collections::HashMap<SessionID, secretbox::Key>;

#[derive(Serialize, Deserialize)]
pub struct SessionData {
    pub id: SessionID,
    pub key: secretbox::Key,
}

pub type FileID = String;

#[derive(Serialize, Deserialize)]
pub struct FileLocationsRequestData {
    pub file_id: FileID,
}

pub type FileLocation = String;

pub type Peer = (FileLocation, box_::PublicKey);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FileReport {
    pub id: FileID,
    pub hash: sodiumoxide::crypto::hash::sha512::Digest,
}

pub struct FileListing {
    pub hash: Vec<u8>,
    pub location: Peer,
}

pub type LocationsList = Vec<Peer>;

#[derive(Serialize, Deserialize, Debug)]
pub struct FileLocations {
    pub data: LocationsList,
}

#[derive(Serialize, Deserialize)]
pub struct FileAvailability {
    pub address: FileLocation,
    pub public_key: box_::PublicKey,
    pub files: Vec<FileReport>,
}

pub type EncryptedData = Vec<u8>;

pub enum DataType {
    SessionRequest(SessionRequestData),
    Session(SessionData),
    FileLocationsRequest(FileLocationsRequestData),
    FileLocations(FileLocations),
}

#[derive(Serialize, Deserialize)]
pub enum ClientToServerType {
    PublicKeyRequest,
    SessionRequest(box_::PublicKey, box_::Nonce, EncryptedData),
    AvailableFilesReport(SessionID, secretbox::Nonce, EncryptedData),
    FileLocationsRequest(SessionID, secretbox::Nonce, EncryptedData),
}

#[derive(Serialize, Deserialize)]
pub enum ServerToClientType {
    PublicKeyReport(box_::PublicKey),
    SessionReport(box_::Nonce, EncryptedData),
    AvailabilityConfirmation(Vec<FileReport>),
    FileLocationsReport(SessionID, secretbox::Nonce, EncryptedData),
}

#[derive(Serialize, Deserialize)]
pub struct FileRequestData {
    pub file_id: String,
    pub session_key: secretbox::Key,
    pub public_key: box_::PublicKey,
    pub session_id: SessionID,
}

#[derive(Serialize, Deserialize)]
pub struct FileRequest {
    pub public_key: box_::PublicKey,
    pub data: EncryptedData,
}

#[derive(Serialize, Deserialize)]
pub struct FileTransmission {
    pub session_id: SessionID,
    pub transmission_nonce: secretbox::Nonce,
    pub signature_nonce: box_::Nonce,
    pub signed_hash: EncryptedData,
    pub file_cipher: EncryptedData,
}

#[derive(Serialize, Deserialize)]
pub enum PeerToPeerType {
    FileRequest(FileRequest),
    FileTransmission(FileTransmission),
}

#[derive(Serialize, Deserialize)]
pub enum RequestType {
    ClientToServer(ClientToServerType),
    ServerToClient(ServerToClientType),
    PeerToPeerType(PeerToPeerType),
}

pub fn pack_request(mut req_bytes_intermediate: Vec<u8>) -> Vec<u8> {
    let req_size = req_bytes_intermediate.len() as u64;
    println!("Sending request of size {} bytes", req_size);
    let size_bytes: [u8; 8] = unsafe { std::mem::transmute(req_size) };
    let mut req_bytes = size_bytes.to_vec();
    req_bytes.append(&mut req_bytes_intermediate);
    req_bytes
}

pub fn unpack_request(mut stream: &std::net::TcpStream) -> Option<Vec<u8>> {
    let mut size_raw: [u8; 8] = [0; 8];
    match stream.read_exact(&mut size_raw) {
        Ok(_) => (),
        Err(e) => {
            println!("Problem reading size from stream: \n{}", e);
            return None;
        }
    }

    let size: u64 = unsafe { std::mem::transmute(size_raw) };
    println!("Specified size for incoming request is {} bytes", size);

    let mut bytes: Vec<u8> = vec![0; size as usize];
    match stream.read_exact(&mut bytes) {
        Ok(_) => (),
        Err(e) => {
            println!("Problem reading request from stream:\n{}", e);
            return None;
        }
    }
    Some(bytes)
}

/*
*  Thread pool implementation from
*  chapter 20 of "The Rust Programming Language":
*  https://doc.rust-lang.org/stable/book/second-edition/
*/

enum Message {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<FnBox + Send + 'static>;

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }

    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(Message::NewJob(job)).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let message = receiver.lock().unwrap().recv().unwrap();

            match message {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);

                    job.call_box();
                }
                Message::Terminate => {
                    println!("Worker {} was told to terminate.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}
