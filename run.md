#!/bin/bash

1. docker run --network="host" p2pserver
2. docker run --network="host" p2pclient1
3. docker run -ti --network="host" p2pclient2 /bin/sh
    -> (in docker container shell): cargo run 8282 1

To stop all containers: docker container kill $(docker ps -a -q)