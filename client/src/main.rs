extern crate p2p_common;
extern crate serde_json;
extern crate sodiumoxide;

use sodiumoxide::crypto::{box_, hash, secretbox};
use std::io;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;

use p2p_common::{ClientToServerType::*, PeerToPeerType::*, ServerToClientType::*, *};
// TODO: Verify that everything is safe from replay attacks (do we need to exncrypt nonces?)

fn main() {
    sodiumoxide::init();

    //let pool = ThreadPool::new(4);

    loop {
        let port = match std::env::args().nth(1) {
            Some(a) => a,
            None => {
                println!("Must provide port number as argument");
                break;
            }
        };

        fn handle_file_report(files: &mut Vec<FileReport>, file_id: String) {
            match std::fs::File::open(&file_id) {
                Ok(file) => {
                    let mut file = file;
                    let mut file_bytes: Vec<u8> = vec![];
                    match file.read_to_end(&mut file_bytes) {
                        Ok(_) => (),
                        Err(e) => {
                            println!("Unable to read local file: \n{}", e);
                        }
                    }
                    let file_hash = hash::hash(&file_bytes);
                    files.push(FileReport {
                        id: file_id,
                        hash: file_hash,
                    });
                }
                Err(_) => {
                    println!("Could not open file '{}', skipping", file_id);
                }
            }
        }

        let (public_key, private_key) = box_::gen_keypair();

        let args_count = std::env::args().count();
        let files_to_share: Vec<FileReport> = match args_count > 2 {
            true => {
                let mut files = vec![];
                for n in 2..args_count {
                    let name = match std::env::args().nth(n) {
                        Some(a) => a,
                        None => {
                            println!("Problem getting file name from cmd line");
                            continue;
                        }
                    };
                    handle_file_report(&mut files, name);
                }
                files
            }
            false => vec![],
        };

        // 'pat' or 'pam' or 'kim'
        println!("Username: ");
        let mut username = String::new();
        match io::stdin().read_line(&mut username) {
            Ok(_) => (),
            Err(e) => {
                println!("Unable to read username from cmd line: \n{}", e);
                break;
            }
        }

        // '12345' or 'password' or 'iamkim'
        println!("Password: ");
        let mut password = String::new();
        match io::stdin().read_line(&mut password) {
            Ok(_) => (),
            Err(e) => {
                println!("Unable to read password from cmd line: \n{}", e);
                break;
            }
        }

        username = username
            .replace("\n", "")
            .replace("\r", "")
            .trim()
            .to_string();
        password = password
            .replace("\n", "")
            .replace("\r", "")
            .trim()
            .to_string();

        let user = SessionRequestData { username, password };

        //let mut sessions = Box::new(SessionMap::new());

        let server_address = "127.0.0.1:7878".to_string();
        let own_address = "127.0.0.1:".to_string() + &port;

        println!("Requesting server's public key");
        let public_key_report = process(&server_address, PublicKeyRequest);
        println!("Public key report received");

        let public_key_report = match public_key_report {
            Some(r) => r,
            None => break,
        };
        let server_public_key = match public_key_report {
            PublicKeyReport(key) => key,
            _ => {
                println!("Invalid key report");
                break;
            }
        };

        let nonce_1 = box_::gen_nonce();

        let req_data_cipher = box_::seal(
            &serde_json::to_vec(&user).unwrap(),
            &nonce_1,
            &server_public_key,
            &private_key,
        );

        println!("Sending session request");
        let session_report = process(
            &server_address,
            SessionRequest(public_key, nonce_1, req_data_cipher),
        );

        let session_report = match session_report {
            Some(r) => r,
            None => break,
        };
        let (session_rep_nonce, session_rep_cipher) = match session_report {
            SessionReport(nonce, key_data) => (nonce, key_data),
            _ => {
                println!("Invalid key report");
                break;
            }
        };

        let session_data_raw = match box_::open(
            &session_rep_cipher,
            &session_rep_nonce,
            &server_public_key,
            &private_key,
        ) {
            Ok(c) => c,
            Err(_) => {
                println!("Unable to decrypt session key");
                break;
            }
        };
        let session_data: SessionData = match serde_json::from_slice(&session_data_raw) {
            Ok(c) => c,
            Err(e) => {
                println!("Unable to parse session key data: \n{}", e);
                break;
            }
        };
        println!("Received session information");

        let mut file_names: Vec<String> = vec![];
        for file in files_to_share.clone() {
            file_names.push(file.id);
        }
        if file_names.len() > 0 {
            println!(
                "Reporting files {:?} available at '{}'",
                file_names, own_address
            );
        } else {
            println!("Reporting 0 files");
        }
        let available_files_data = FileAvailability {
            address: own_address.clone(),
            public_key: public_key,
            files: files_to_share.clone(),
        };
        let af_bytes = match serde_json::to_vec(&available_files_data) {
            Ok(b) => b,
            Err(e) => {
                println!("Unable to convert available files data to bytes:\n{}", e);
                break;
            }
        };
        let nonce_2 = secretbox::gen_nonce();
        let af_cipher = secretbox::seal(&af_bytes, &nonce_2, &session_data.key);

        let files_report_confirmation = process(
            &server_address,
            AvailableFilesReport(session_data.id, nonce_2, af_cipher),
        );

        let files_report_confirmation = match files_report_confirmation {
            Some(c) => c,
            None => break,
        };
        let files_list = match files_report_confirmation {
            AvailabilityConfirmation(list) => list,
            _ => {
                println!("Cannot verify that availability report was accepted");
                break;
            }
        };

        println!("Received list of available files");

        let mut done = false;

        while !done {
            let mut choice = String::new();
            let mut input_is_valid = false;
            let mut request_file = false;
            while !input_is_valid {
                println!("Would you like to request a file? (y/n): ");
                match io::stdin().read_line(&mut choice) {
                    Ok(_) => (),
                    Err(e) => {
                        println!("Problem getting cmd line input: \n{}", e);
                        continue;
                    }
                }
                choice = choice
                    .replace("\n", "")
                    .replace("\r", "")
                    .trim()
                    .to_string();
                match &choice[..] {
                    "y" => {
                        input_is_valid = true;
                        request_file = true;
                    }
                    "n" => {
                        println!("Got it, moving on...");
                        input_is_valid = true;
                        request_file = false;
                        done = true;
                    }
                    _ => (),
                }
            }

            /*
    println!("Please specify index of file you would like: ");

    let mut choice = String::new();
    io::stdin().read_line(&mut choice).unwrap();

    let choice: usize = choice.parse().expect("Please enter a number");
    */
            if request_file {
                println!("Here is a list of available files:");
                for n in 0..files_list.len() {
                    println!("( {} ) : '{}'", n + 1, files_list[n].id);
                }

                let mut choice_s = String::new();
                let mut choice: usize = 0;

                let mut input_is_valid = false;
                while !input_is_valid {
                    println!("Please specify the number of which file you would like:");
                    match io::stdin().read_line(&mut choice_s) {
                        Ok(_) => (),
                        Err(e) => {
                            println!("Unable to get file choice: \n{}", e);
                            continue;
                        }
                    }
                    choice_s = choice_s
                        .replace("\n", "")
                        .replace("\r", "")
                        .trim()
                        .to_string();
                    println!("You chose '{}'", choice_s);
                    match choice_s.parse::<usize>() {
                        Ok(val) => {
                            choice = val - 1;
                            if choice <= files_list.len() {
                                input_is_valid = true;
                            } else {
                                println!("Invalid choice");
                            }
                        }
                        Err(_) => {
                            println!("Invalid choice");
                            choice = 0;
                        }
                    };
                }

                let chosen_file = &files_list[choice];

                let nonce_3 = secretbox::gen_nonce();
                println!(
                    "Preparing to request locations for file with id: '{}'",
                    chosen_file.id
                );

                let locations_request_data = FileLocationsRequestData {
                    file_id: chosen_file.id.clone(),
                };
                let loc_req_bytes = match serde_json::to_vec(&locations_request_data) {
                    Ok(c) => c,
                    Err(e) => {
                        println!(
                            "Unable to convert file location request data to bytes: \n{}",
                            e
                        );
                        break;
                    }
                };
                let loc_req_data_cipher =
                    secretbox::seal(&loc_req_bytes[..], &nonce_3, &session_data.key);

                let file_locations_report = process(
                    &server_address,
                    FileLocationsRequest(session_data.id, nonce_3, loc_req_data_cipher),
                );

                println!("Received list of file locations");

                let file_locations_report = match file_locations_report {
                    Some(r) => r,
                    None => break,
                };
                let (locations_rep_nonce, locations_rep_cipher) = match file_locations_report {
                    FileLocationsReport(_session_id, nonce, locations_cipher) => {
                        (nonce, locations_cipher)
                    }
                    _ => {
                        println!("Invalid key report");
                        break;
                    }
                };
                let locations_raw_bytes = match secretbox::open(
                    &locations_rep_cipher,
                    &locations_rep_nonce,
                    &session_data.key,
                ) {
                    Ok(b) => b,
                    Err(_) => {
                        println!("Unable to decrypt file locations data");
                        break;
                    }
                };
                let locations: FileLocations =
                    match serde_json::from_slice(&locations_raw_bytes[..]) {
                        Ok(l) => l,
                        Err(e) => {
                            println!("Unable to parse locations data:\n{}", e);
                            break;
                        }
                    };

                let (file_address, peer_public_key) = locations.data[0].clone();

                let session_key = secretbox::gen_key();

                let mut session_id: [u8; 4] = [0; 4];
                let random_bytes = sodiumoxide::randombytes::randombytes(4);

                // Method based on https://stackoverflow.com/a/29784723
                for (place, element) in session_id.iter_mut().zip(random_bytes[0..4].iter()) {
                    *place = *element;
                }

                let rdc_bytes = &serde_json::to_vec(&FileRequestData {
                    file_id: chosen_file.id.clone(),
                    session_key: session_key.clone(),
                    public_key: public_key.clone(),
                    session_id,
                });
                let rdc_bytes = match rdc_bytes {
                    Ok(b) => b,
                    Err(e) => {
                        println!(
                            "Unable to convert file request data cipher to bytes:\n{}",
                            e
                        );
                        break;
                    }
                };
                let req_data_cipher =
                    sodiumoxide::crypto::sealedbox::seal(rdc_bytes, &peer_public_key);

                println!("Requesting file '{}' from '{}'", choice, file_address);

                let file_response = process_p2p_1(
                    &file_address,
                    p2p_common::FileRequest {
                        public_key: public_key,
                        data: req_data_cipher,
                    },
                );
                let file_response = match file_response {
                    Some(r) => r,
                    None => break,
                };
                println!("Received file data");

                let file_data = {
                    if file_response.session_id == session_id {
                        println!("Decrypting...");
                        let file_bytes = match secretbox::open(
                            &file_response.file_cipher,
                            &file_response.transmission_nonce,
                            &session_key,
                        ) {
                            Ok(b) => b,
                            Err(_) => {
                                println!("Unable to decrypt file bytes");
                                break;
                            }
                        };
                        let computed_hash = sodiumoxide::crypto::hash::hash(&file_bytes);
                        let reported_hash = chosen_file.hash;
                        if reported_hash != computed_hash {
                            println!("Unable to verify file");
                            break;
                        }
                        file_bytes
                    } else {
                        println!("Invalid session id");
                        break;
                    }
                };

                let mut out_file = match std::fs::File::create(&chosen_file.id) {
                    Ok(f) => f,
                    Err(e) => {
                        println!("Could not create local file:\n{}", e);
                        break;
                    }
                };
                match out_file.write_all(&file_data) {
                    Ok(_) => (),
                    Err(e) => {
                        println!("Unable to write file bytes to disk:\n{}", e);
                        break;
                    }
                }
            } else if files_to_share.len() > 0 {
                let mut choice = String::new();
                let mut input_is_valid = false;
                let mut share_files = false;

                while !input_is_valid {
                    println!("Would you like to share files? (y/n): ");
                    match io::stdin().read_line(&mut choice) {
                        Ok(_) => (),
                        Err(e) => {
                            println!("Problem getting cmd line input: \n{}", e);
                            continue;
                        }
                    }
                    choice = choice
                        .replace("\n", "")
                        .replace("\r", "")
                        .trim()
                        .to_string();
                    match &choice[..] {
                        "y" => {
                            input_is_valid = true;
                            share_files = true;
                        }
                        "n" => {
                            input_is_valid = true;
                            done = true;
                        }
                        _ => (),
                    }
                }

                if share_files {
                    let listener = match TcpListener::bind(&own_address) {
                        Ok(l) => l,
                        Err(e) => {
                            println!("Couldn't bind to port:\n{}", e);
                            break;
                        }
                    };
                    println!("Now listenting for file requests at {}", own_address);
                    for stream in listener.incoming() {
                        let stream = match stream {
                            Ok(s) => s,
                            Err(e) => {
                                println!("Couldn't get stream:\n{}", e);
                                break;
                            }
                        };
                        handle_connection(
                            &stream,
                            &public_key,
                            &private_key,
                            &files_to_share
                            //&mut sessions,
                            //&mut files,
                        );
                        //println!("Sessions list: {:?}", sessions);
                    }
                }
            }
            if done {
                println!("Bye!");
                break;
            }
        }
        break;
    }
}

fn handle_connection(
    mut stream: &TcpStream,
    public_key: &box_::PublicKey,
    private_key: &box_::SecretKey,
    files_to_share: &Vec<FileReport>,
) {
    println!("New connection, likely a file request");
    let file_request = match handle_peer_request(stream) {
        Some(r) => r,
        None => return,
    };
    let file_request = match file_request {
        FileRequest(request) => request,
        _ => {
            println!("Invalid request");
            return;
        }
    };
    let request_data =
        match sodiumoxide::crypto::sealedbox::open(&file_request.data, &public_key, &private_key) {
            Ok(d) => d,
            Err(_) => {
                println!("Unable to decrypt file request data");
                return;
            }
        };
    let request_data: p2p_common::FileRequestData = match serde_json::from_slice(&request_data) {
        Ok(d) => d,
        Err(e) => {
            println!("Unable to parse file request data: \n{}", e);
            return;
        }
    };

    let mut file_listing = None;
    let mut have_file = false;
    for item in files_to_share {
        if request_data.file_id == item.id {
            file_listing = Some(item.clone());
            have_file = true;
        }
    }

    if have_file {
        let file_listing = file_listing.unwrap();
        let mut file = match std::fs::File::open(file_listing.id) {
            Ok(f) => f,
            Err(e) => {
                println!("Unable to open local file from disk: \n{}", e);
                return;
            }
        };
        let mut file_bytes: Vec<u8> = vec![];
        match file.read_to_end(&mut file_bytes) {
            Ok(_) => (),
            Err(e) => {
                println!("Unable to read local file:\n{}", e);
                return;
            }
        }

        let file_hash = file_listing.hash;
        let signature_nonce = box_::gen_nonce();
        let signed_hash = box_::seal(
            &file_hash[..],
            &signature_nonce,
            &file_request.public_key,
            &private_key,
        );
        let transmission_nonce = secretbox::gen_nonce();
        let file_cipher =
            secretbox::seal(&file_bytes, &transmission_nonce, &request_data.session_key);
        let file_transmission =
            prepare_p2p_request(FileTransmission(p2p_common::FileTransmission {
                session_id: request_data.session_id,
                transmission_nonce,
                signature_nonce,
                signed_hash,
                file_cipher,
            }));
        let file_transmission = match file_transmission {
            Some(t) => t,
            None => {
                return;
            }
        };
        println!("Sending file");
        match stream.write_all(&file_transmission) {
            Ok(_) => (),
            Err(_) => {
                println!("Unable to send file transmission");
                return;
            } //stream.flush().unwrap();
        }
    } else {
        println!("Requested file is not available");
        return;
    }
}

fn prepare_request(req_struct: ClientToServerType) -> Option<Vec<u8>> {
    let bytes_intermediate = match serde_json::to_vec(&req_struct) {
        Ok(b) => b,
        Err(e) => {
            println!("Couldn't convert request struct to bytes:\n{}", e);
            return None;
        }
    };
    Some(pack_request(bytes_intermediate))
}

fn process(address: &String, request: ClientToServerType) -> Option<ServerToClientType> {
    let mut stream = match TcpStream::connect(address) {
        Ok(s) => s,
        Err(e) => {
            println!("Unable to connect to server:\n{}", e);
            return None;
        }
    };

    let req_bytes = match prepare_request(request) {
        Some(b) => b,
        None => return None,
    };

    match stream.write_all(&req_bytes) {
        Ok(_) => (),
        Err(e) => {
            println!("Unable to send request:\n{}", e);
            return None;
        }
    }

    handle_server_request(stream)
}

fn handle_server_request(stream: TcpStream) -> Option<ServerToClientType> {
    let bytes = match unpack_request(&stream) {
        Some(b) => b,
        None => return None,
    };
    match serde_json::from_slice(&bytes) {
        Ok(r) => Some(r),
        Err(e) => {
            println!("Unable to parse request from server: \n{}", e);
            None
        }
    }
}

fn prepare_p2p_request(req_struct: PeerToPeerType) -> Option<Vec<u8>> {
    let bytes_intermediate = match serde_json::to_vec(&req_struct) {
        Ok(b) => b,
        Err(e) => {
            println!("Couldn't convert request struct to bytes: \n{}", e);
            return None;
        }
    };
    Some(pack_request(bytes_intermediate))
}

fn process_p2p_1(
    address: &String,
    request: p2p_common::FileRequest,
) -> Option<p2p_common::FileTransmission> {
    println!("Attempting to connect to peer at address {}", address);
    let mut stream = match TcpStream::connect(address) {
        Ok(s) => s,
        Err(e) => {
            println!("Unable to connect to server:\n{}", e);
            return None;
        }
    };

    let req_bytes = prepare_p2p_request(FileRequest(request));

    let req_bytes = match req_bytes {
        Some(b) => b,
        None => return None,
    };

    match stream.write_all(&req_bytes) {
        Ok(_) => (),
        Err(e) => {
            println!("Unable to send public key request:\n{}", e);
            return None;
        }
    }

    let peer_transmission = match handle_peer_transmission(stream) {
        Some(t) => t,
        None => return None,
    };
    match peer_transmission {
        FileTransmission(file_transmission) => Some(file_transmission),
        _ => {
            println!("Unable to parse file transmission");
            None
        }
    }
}

fn handle_peer_transmission(stream: TcpStream) -> Option<PeerToPeerType> {
    let bytes = match unpack_request(&stream) {
        Some(b) => b,
        None => return None,
    };
    match serde_json::from_slice(&bytes) {
        Ok(b) => Some(b),
        Err(e) => {
            println!("Unable to parse request from peer: \n{}", e);
            None
        }
    }
}

fn handle_peer_request(stream: &TcpStream) -> Option<PeerToPeerType> {
    let bytes = match unpack_request(stream) {
        Some(b) => b,
        None => return None,
    };
    match serde_json::from_slice(&bytes) {
        Ok(b) => Some(b),
        Err(e) => {
            println!("Unable to parse request from client:\n{}", e);
            None
        }
    }
}
