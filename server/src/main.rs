extern crate p2p_common;
extern crate serde_json;
extern crate sodiumoxide;
extern crate unqlite;

use sodiumoxide::crypto::{box_, hash, pwhash, secretbox};
use std::collections::HashMap;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::sync::{Arc, Mutex};
use unqlite::{UnQLite, KV};

use p2p_common::{ClientToServerType::*, ServerToClientType::*, *};

type FilesMap = HashMap<FileID, (hash::sha512::Digest, LocationsList)>;

fn main() {
    let db = Arc::new(UnQLite::create("server.db"));

    let listener = TcpListener::bind("127.0.0.1:7878").expect("Couldn't bind to port :/");
    let _ = sodiumoxide::init();
    let (public_key, private_key) = box_::gen_keypair();
    let public_key = Arc::new(public_key);
    let private_key = Arc::new(private_key);

    let pool = ThreadPool::new(4);

    let sessions_map = SessionMap::new();
    let sessions = Arc::new(Mutex::new(sessions_map));
    let files = Arc::new(Mutex::new(FilesMap::new()));

    for stream in listener.incoming() {
        let stream = match stream {
            Ok(stream) => stream,
            Err(e) => {
                println!("Couldn't get stream: \n{}", e);
                continue;
            }
        };
        let public_key = Arc::clone(&public_key);
        let private_key = Arc::clone(&private_key);
        let sessions = Arc::clone(&sessions);
        let files = Arc::clone(&files);
        let db = Arc::clone(&db);
        pool.execute(move || {
            handle_connection(stream, public_key, private_key, sessions, files, db);
        });
    }
}

fn handle_connection(
    mut stream: TcpStream,
    public_key: Arc<box_::PublicKey>,
    private_key: Arc<box_::SecretKey>,
    sessions: Arc<Mutex<SessionMap>>,
    files: Arc<Mutex<FilesMap>>,
    db: Arc<UnQLite>,
) {
    println!("Received a request");
    let incoming_request = handle_client_request(&stream);
    loop {
        let incoming_request = match incoming_request {
            Some(r) => r,
            None => break,
        };
        match incoming_request {
            PublicKeyRequest => {
                println!("Client asking for server's public key");

                let report_bytes = prepare_request(PublicKeyReport(*public_key));
                let report_bytes = match report_bytes {
                    Some(b) => b,
                    None => break,
                };

                match stream.write(&report_bytes) {
                    Ok(_) => (),
                    Err(e) => println!("Couldn't send key report: \n{}", e),
                }
                stream.flush().unwrap();
            }
            SessionRequest(client_public_key, the_nonce, contents) => {
                println!("Client asking for session");

                let contents_plain =
                    match box_::open(&contents, &the_nonce, &client_public_key, &private_key) {
                        Ok(c) => c,
                        Err(_) => {
                            println!("Unable to decrypt session request");
                            break;
                        }
                    };
                let contents_parsed: SessionRequestData =
                    match serde_json::from_slice(&contents_plain[..]) {
                        Ok(c) => c,
                        Err(e) => {
                            println!("Unable to parse session request: \n{}", e);
                            break;
                        }
                    };

                let session_key = secretbox::gen_key();
                let mut session_id: [u8; 4] = [0; 4];
                let random_bytes = sodiumoxide::randombytes::randombytes(4);

                // Method based on https://stackoverflow.com/a/29784723
                for (place, element) in session_id.iter_mut().zip(random_bytes[0..4].iter()) {
                    *place = *element;
                }

                let mut sessions = sessions.lock().unwrap();
                sessions.insert(session_id, session_key.clone());

                println!("Request is from user '{}'", contents_parsed.username);
                let username_hash = hash::hash(contents_parsed.username.as_bytes());

                if !db.kv_contains(&username_hash[..]) {
                    println!("Unknown user");
                    break;
                }

                let pwhash = db.kv_fetch(&username_hash[..]).unwrap();
                let pwhash = pwhash::HashedPassword::from_slice(&pwhash[..]).unwrap();
                let password_bytes = contents_parsed.password.as_bytes();
                if !pwhash::pwhash_verify(&pwhash, password_bytes) {
                    println!("Incorrect password");
                    break;
                }

                println!("Sending session key");
                let session_rep_data = SessionData {
                    id: session_id,
                    key: session_key.clone(),
                };
                let session_rep_data_bytes = match serde_json::to_vec(&session_rep_data) {
                    Ok(c) => c,
                    Err(e) => {
                        println!("Unable to convert session key data to vec: \n{}", e);
                        break;
                    }
                };
                let session_rep_nonce = box_::gen_nonce();
                let session_rep_data_cipher = box_::seal(
                    &session_rep_data_bytes[..],
                    &session_rep_nonce,
                    &client_public_key,
                    &private_key,
                );
                let session_rep_bytes =
                    prepare_request(SessionReport(session_rep_nonce, session_rep_data_cipher));
                let session_rep_bytes = match session_rep_bytes {
                    Some(b) => b,
                    None => break,
                };

                match stream.write_all(&session_rep_bytes) {
                    Ok(_) => (),
                    Err(e) => println!("Unable to send session key: \n{}", e),
                }
                stream.flush().unwrap();
            }
            AvailableFilesReport(session_id, report_nonce, cipher) => {
                println!("Received available files report");
                let sessions = sessions.lock().unwrap();
                if sessions.contains_key(&session_id) {
                    let session_key = match sessions.get(&session_id) {
                        Some(key) => key,
                        None => {
                            println!("Invalid session id");
                            break;
                        }
                    };
                    let request_raw_bytes =
                        match secretbox::open(&cipher, &report_nonce, &session_key) {
                            Ok(b) => b,
                            Err(_) => {
                                println!("Unable to decrypt file locations request");
                                break;
                            }
                        };
                    let file_availability: FileAvailability =
                        match serde_json::from_slice(&request_raw_bytes) {
                            Ok(c) => c,
                            Err(e) => {
                                println!("Unable to parse available files report: \n{}", e);
                                break;
                            }
                        };

                    for file in file_availability.files.iter() {
                        let location = (
                            file_availability.address.clone(),
                            file_availability.public_key.clone(),
                        );
                        let mut files = files.lock().unwrap();
                        println!("Going to add file information to list");
                        if files.contains_key(&file.id) {
                            println!("Adding new location for file: '{}'", file.id);
                            let (_, mut locations) = match files.get(&file.id) {
                                Some(c) => c.clone(),
                                None => {
                                    println!("Ding dong something is wrong");
                                    break;
                                }
                            };
                            locations.push(location);
                            files.insert(file.id.clone(), (file.hash, locations));
                        } else {
                            println!("Adding '{}'", file.id);
                            files.insert(file.id.clone(), (file.hash, vec![location]));
                        }
                    }

                    println!("Files list has been updated");

                    // Based on https://stackoverflow.com/questions/30933567/create-a-vector-from-iterating-hashmap
                    let mut files = files.lock().unwrap();
                    let mut files_list: Vec<FileReport> = vec![];
                    for (key, (hash, _)) in files.iter() {
                        files_list.push(FileReport {
                            id: key.clone(),
                            hash: hash.clone(),
                        });
                    }
                    println!("Sending confirmation with files list");
                    let report = prepare_request(AvailabilityConfirmation(files_list));
                    let report = match report {
                        Some(r) => r,
                        None => break,
                    };
                    match stream.write_all(&report) {
                        Ok(_) => (),
                        Err(e) => println!("Unable to send availability confirmation: \n{}", e),
                    }
                    stream.flush().unwrap();
                } else {
                    match stream.write_all(b"Invalid session id") {
                        Ok(_) => (),
                        Err(e) => println!("Unable to report invalid id: \n{}", e),
                    }
                    stream.flush().unwrap();
                }
            }
            FileLocationsRequest(session_id, request_nonce, cipher) => {
                println!("Received request for file locations");
                let mut sessions = sessions.lock().unwrap();
                if sessions.contains_key(&session_id) {
                    let session_key = sessions.get(&session_id).unwrap();
                    let request_raw_bytes =
                        match secretbox::open(&cipher, &request_nonce, &session_key) {
                            Ok(c) => c,
                            Err(_) => {
                                println!("Unable to decrypt file locations request");
                                break;
                            }
                        };
                    let locations_request: FileLocationsRequestData =
                        match serde_json::from_slice(&request_raw_bytes[..]) {
                            Ok(c) => c,
                            Err(e) => {
                                println!("Unable to parse file locations request: \n{}", e);
                                break;
                            }
                        };
                    println!("About to send locations for file");
                    let files = files.lock().unwrap();
                    let locations_data = match files.get(&locations_request.file_id) {
                        Some((_, list)) => list.clone(),
                        None => vec![],
                    };
                    let locations = FileLocations {
                        data: locations_data,
                    };
                    let locations_bytes = match serde_json::to_vec(&locations) {
                        Ok(c) => c,
                        Err(e) => {
                            println!("Unable to convert locations data to bytes: \n{}", e);
                            break;
                        }
                    };
                    let locations_rep_nonce = secretbox::gen_nonce();
                    let locations_cipher =
                        secretbox::seal(&locations_bytes[..], &locations_rep_nonce, &session_key);
                    let locations_rep_bytes = prepare_request(FileLocationsReport(
                        session_id,
                        locations_rep_nonce,
                        locations_cipher,
                    ));
                    let locations_rep_bytes = match locations_rep_bytes {
                        Some(b) => b,
                        None => break,
                    };

                    match stream.write_all(&locations_rep_bytes) {
                        Ok(_) => (),
                        Err(e) => println!("Unable to send locations report: \n{}", e),
                    }
                    stream.flush().unwrap();
                // Having trouble getting this to work
                //let _ = sessions.remove(&session_id);
                } else {
                    match stream.write_all(b"Invalid session id") {
                        Ok(_) => (),
                        Err(e) => println!("Unable to report invalid id: \n{}", e),
                    }
                    stream.flush().unwrap();
                }
            }
        }
        break;
    }
    /*
        let response = "HTTP/1.1 200 OK\r\n\r\n";

        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
        */
}

fn prepare_request(req_struct: ServerToClientType) -> Option<Vec<u8>> {
    let bytes_intermediate = match serde_json::to_vec(&req_struct) {
        Ok(b) => b,
        Err(e) => {
            println!("Couldn't convert request struct to bytes:\n{}", e);
            return None;
        }
    };
    Some(pack_request(bytes_intermediate))
}

fn handle_client_request(stream: &TcpStream) -> Option<ClientToServerType> {
    let bytes = match unpack_request(stream) {
        Some(b) => b,
        None => return None,
    };
    match serde_json::from_slice(&bytes) {
        Ok(c) => Some(c),
        Err(e) => {
            println!("Unable to parse request from client: \n{}", e);
            None
        }
    }
}
