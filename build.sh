#!/bin/bash

docker build -f Dockerfile.one --tag p2pclient1 .
docker build -f Dockerfile.two --tag p2pclient2 .
docker build -f Dockerfile.three --tag p2pserver .